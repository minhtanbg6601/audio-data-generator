import os
from pydub import AudioSegment


def split_audio_into_segments(audio_path, segment_duration):
    # Load the audio file
    audio = AudioSegment.from_file(audio_path)

    # Calculate segment duration in milliseconds
    segment_duration_ms = segment_duration * 1000

    # Split the audio into segments
    segments = []
    total_duration = len(audio)
    current_time = 0
    while current_time < total_duration:
        end_time = current_time + segment_duration_ms
        if end_time > total_duration:
            end_time = total_duration
        segment = audio[current_time:end_time]
        segments.append(segment)
        current_time = end_time

    return segments


# Specify input folder path
input_folder = "./output-silent"

# Specify output folder path
output_folder = "./chunked-silent-vocal"
os.makedirs(output_folder, exist_ok=True)
# Duration of each segment in seconds
segment_duration = 5

# Iterate through files in the input folder
for filename in os.listdir(input_folder):
    if filename.endswith('.wav'):
        print('start process',filename)
        input_file_path = os.path.join(input_folder, filename)

        # Create an output folder with the same name as the input file (without extension)
        output_subfolder = os.path.splitext(filename)[0]
        output_subfolder_path = os.path.join(output_folder, output_subfolder)
        os.makedirs(output_subfolder_path, exist_ok=True)

        # Split the audio into segments
        segments = split_audio_into_segments(input_file_path, segment_duration)

        # Export each segment to the output subfolder
        for i, segment in enumerate(segments):
            output_file = f"segment_{i+1}.wav"
            output_file_path = os.path.join(output_subfolder_path, output_file)
            segment.export(output_file_path, format="wav")
        print('done.')
# end