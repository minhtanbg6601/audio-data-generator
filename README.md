## step 1: install packages
```
pip install -r requirement.txt
```
## step 2: copy raw audio file
```
cp -rf (your files) ./audio
```
## step 3: generate
- this step remove background music, silent audio and chunk the output to segment of 5 seconds
```
sh generate_data.sh
```
## get the output
- chunked audio is placed at ./chunked-silent-vocal
