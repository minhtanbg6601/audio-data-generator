import os
from pydub import AudioSegment
from pydub.silence import split_on_silence

def remove_silence_from_folder(folder_path):
    # Iterate through files in the folder
    for filename in os.listdir(folder_path):
        if filename.endswith('.wav'):
            print("working on",filename)
            file_path = os.path.join(folder_path, filename)

            # Load the audio file
            audio = AudioSegment.from_file(file_path, format="wav")

            # Split the audio based on silence
            chunks = split_on_silence(audio, min_silence_len=500, silence_thresh=-40, keep_silence=200)

            # Concatenate non-silent chunks
            output = AudioSegment.silent(duration=0)
            for chunk in chunks:
                output += chunk

            # Define the output file path
            output_file_path = os.path.join('./output-silent/', f"silent_{filename}")

            # Export the output audio
            output.export(output_file_path, format="wav")
            print("done",output_file_path)

# Provide the folder path as an argument
folder_path = "./vocal-output"
os.makedirs('./output-silent', exist_ok=True)
# Call the function to remove silence from all audio files in the folder
remove_silence_from_folder(folder_path)
